import { makeStyles, useTheme } from '@material-ui/core/styles';

const drawerWidth = '100%';

const useStyles = makeStyles((theme) => ({
  navbarRoot: {
    display:'flex',
    flexDirection: 'row',
    nowrap: 'nowrap',
    alignContent: 'flex-start',
    margin:2,
  },
  navbarAppbar: {
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-around',
    [theme.breakpoints.down('xs')]: {
      display:'none',
    },
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  navbarToolbar:{
    width:'100%',
  },
  navbarContent: {
    display:'flex',
    flexDirection:'row',
    alignItems:'center',
    width:'100%',
    // height:'100%',

    [theme.breakpoints.down('xs')]: {
      flexDirection:'column',
    },
  },
  navbarTitle: {
    flexGrow: 1,
  },
  navbarBtn: {
    alignSelf:'flex-start',
    margin:5,

    [theme.breakpoints.down('xs')]: {
      width:'100%',
    },
  },
  navbarLoginBtn: {
    alignSelf:'flex-end',
    margin:5,
  },
  navbarHide: {
    display: 'none',
  },
  navbarOpenBtn: {
    [theme.breakpoints.up('sm')]: {
      display:'none',
    },
    position:'fixed',
    zIndex:2,
  },
  navbarDrawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  navbarDrawerPaper: {
    width: drawerWidth,
  },
  navbarDrawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  
}));

export default useStyles;