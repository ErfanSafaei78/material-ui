import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    contentRoot: {
      backgroundColor:'lightgrey',
      height:1000,
      order:1,
      direction:'ltr',
      margin:2,
    },
    contentContent:{
      margin:15,
      display:'flex',
      flexDirection:'column',
    },
    contentImg: {
      width:'80%',
      height: 'auto',
      alignSelf:'center',
      marginBottom:10
    },
    contentParagraph: {
      fontSize:16
    },
  }));
  
  export default useStyles;