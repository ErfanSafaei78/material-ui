import React from 'react';
import clsx from 'clsx';
import { useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Fab } from '@material-ui/core';
import useStyles from './SidebarUI';
import { Grid } from '@material-ui/core';
import { Paper } from '@material-ui/core';
import { List, ListItemText, ListItem, ListSubheader} from '@material-ui/core';

function SideBarContent() {
  const classes = useStyles();

  return (
    <div className={classes.sidebarContent}>
      <Paper elevation={3} className={classes.sidebarPaper} >
        <Typography variant="h6" noWrap className={classes.sidebarTitle}>
          Sidebar
        </Typography>
        <List className={classes.root} subheader={<li />}>
          {[0, 1, 2, 3, 4].map((sectionId) => (
            <li key={`section-${sectionId}`} className={classes.listSection}>
              <ul className={classes.ul}>
                <ListSubheader>{`I'm sticky ${sectionId}`}</ListSubheader>
                {[0, 1, 2].map((item) => (
                  <ListItem key={`item-${sectionId}-${item}`}>
                    <ListItemText primary={`Item ${item}`} />
                  </ListItem>
                ))}
              </ul>
            </li>
          ))}
        </List>
      </Paper>
    </div>
  );
}
function Sidebar(props) {
  const classes = useStyles();
  const theme = useTheme();

  const {display,setDisplay} = props;
  
  const handleDrawerdisplay = () => {
    setDisplay('sidebar');
  };

  const handleDrawerClose = () => {
    setDisplay('none');
  };

  return (
    <Grid md={4} className={classes.sidebarRoot}>
      <CssBaseline />
      <Fab
        color="inherit"
        aria-label="display drawer"
        edge="end"
        onClick={handleDrawerdisplay}
        className={clsx(display && classes.sidebarHide , classes.sidebarOpenBtn)}
      >
        <MenuIcon />
      </Fab>
      <Grid container
        className={clsx(classes.sidebarContainer)}
      >
        <SideBarContent />
      </Grid>
      <Drawer
        className={classes.sidebarDrawer}
        variant="persistent"
        anchor="right"
        open={display}
        classes={{
          paper: classes.sidebarDrawerPaper,
        }}
      >
        <div className={classes.sidebarDrawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />

        <SideBarContent />

      </Drawer>
    </Grid>
  );
}
export default Sidebar;