import { makeStyles, useTheme } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  sidebarRoot: {
    margin:2,
  },
  sidebarContainer: {
    [theme.breakpoints.down('sm')]: {
      display:'none',
    },
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,

    }),
  },
  sidebarContent: {
    width:'100%',
    height: '1000',
  },
  sidebarPaper: {
    width:'100%',
    height: '1000',
  },
  sidebarTitle: {
    flexGrow: 1,
  },
  sidebarHide: {
    display: 'none',
  },
  sidebarOpenBtn: {
    [theme.breakpoints.up('md')]: {
      display:'none',
    },
    [theme.breakpoints.up('sm')]: {
      left:'96.5%',
    },
    zIndex:2,
    position:'fixed',
    top:'50%',
    left:'94%',
  },
  sidebarDrawer: {
    width: '100%',
    flexShrink: 0,
    [theme.breakpoints.up('sm')]: {
      width: '25%',
    },
  },
  sidebarDrawerPaper: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '34%',
    },
  },
  sidebarDrawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
}));

export default useStyles;