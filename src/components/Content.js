import { Grid, Typography } from "@material-ui/core";
import useStyles from './ContentUI';
import pic02 from '../images/pic02.JPEG'

function Content() {
  const classes = useStyles();
  return (
    <> 
      <Grid container className={classes.contentRoot} > 
        <div className={classes.contentContent}>
          <h1> Ford Mustang Mach 1 1970</h1>
          <img src={pic02} className={classes.contentImg}></img>
          <Typography paragraph direction={'rtl'} className={classes.contentParagraph}>
            for example <br/>
            I posted a photo of a 1970 Ford Mustang Match 1 taken by myself. <br />
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
          </Typography>
        </div>
      </Grid>
    </>
  );
}

export default Content;
