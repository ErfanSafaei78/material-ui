import React from 'react';
import clsx from 'clsx';
import {useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { Fab, Grid } from '@material-ui/core';
import useStyles from './NavbarUI';
import { Button } from '@material-ui/core';


function NavBarContent() {
  const classes = useStyles();

  return (
    <div className={classes.navbarContent}>
      <Typography variant="h6" noWrap className={classes.navbarTitle}>
        Navbar
      </Typography>
      <Grid item xs={12} sm={2}>
        <Button className={classes.navbarBtn} color="inherit" variant="outlined" size={'small'}>Home</Button>
      </Grid>
      <Grid item xs={12} sm={2}>
        <Button className={classes.navbarBtn} color="inherit" variant="outlined" size={'small'} >Contact</Button>
      </Grid>
      <Grid item xs={12} sm={2}>
        <Button className={classes.navbarBtn} color="inherit" variant="outlined" size={'small'}>About</Button>
      </Grid>
      <Grid item xs={12} sm={2}>
        <Button className={classes.navbarLoginBtn, classes.navbarBtn} color="inherit" variant="outlined" size={'small'}>Login</Button>
      </Grid>
    </div>
  );
}
function Navbar(props) {
  const classes = useStyles();
  const theme = useTheme();

  const {display,setDisplay} = props;
  
  const handleDrawerdisplay = () => {
    setDisplay('navbar');
  };

  const handleDrawerClose = () => {
    setDisplay('none');
  };

  return (
    <Grid  className={classes.navbarRoot}>
      <CssBaseline />
      <Fab
        color="primary"
        aria-label="display drawer"
        edge="end"
        onClick={handleDrawerdisplay}
        className={clsx(display && classes.navbarHide , classes.navbarOpenBtn)}
      >
        <MenuIcon />
      </Fab>
      <AppBar position="static" className={clsx(classes.navbarAppbar)} >
      
        <Toolbar className={classes.navbarToolbar}>
          <NavBarContent />
        </Toolbar>
      </AppBar>
      
      <Drawer
        className={classes.navbarDrawer}
        variant="persistent"
        anchor="right"
        open={display}
        classes={{
          paper: classes.navbarDrawerPaper,
        }}
      >
        <div className={classes.navbarDrawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />

        <NavBarContent />

      </Drawer>
    </Grid>
  );
}
export default Navbar;