import { useState } from "react";
import Navbar from "./components/Navbar";
import Content from "./components/Content";
import Sidebar from "./components/Sidebar";
import useStyles from './AppUI';
import { Grid } from "@material-ui/core";


function App() {
  const [display,setDisplay] = useState("none");
  console.log(display);
  const classes = useStyles();
  
  return (
    <div className={classes.appRoot}> 
      <Navbar display = {display === "navbar"} setDisplay = {setDisplay} />
      <div className={classes.appContent}>
        <Content />
        <Sidebar display = {display === "sidebar"} setDisplay = {setDisplay} />
      </div>
    </div>
  );
}

export default App;
