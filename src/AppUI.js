import { makeStyles, useTheme } from '@material-ui/core/styles';
import { positions } from '@material-ui/system';

const useStyles = makeStyles((theme) => ({
    appRoot: {
        direction:'rtl',
    },
    appContent: {
        display:'flex',
        flexDirection:'row',
    }
  }));
  
  export default useStyles;